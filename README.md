[sphinxsearch](https://github.com/fheyer/sphinxsearch)
============

http://www.cnblogs.com/yjf512/p/3598332.html

This magento extension provides integration for Magento CE with a sphinx search server.

Versions
--------
sphinxsearch was tested on Magento CE version 1.7.X.X, 1.8.0.0 and 1.9.3.2.
It probably works with 1.6.X.X too, but i haven't tested it yet.
Earlier versions of Magento won't work without changes in the extension code. 

Installation
------------
* Use modman (https://github.com/colinmollenhour/modman) for easy installation of this extension.
Integration with magento connect will be provided in the near future.
* If you don't want to use modman you can install the extension manually instead:
  * Just copy the following directory with all contained files and subdirs to your shop root directory. Use this exact path, create subdirs if necessary!
    * `app/code/community/Gfe/SphinxSearch`
  * Also copy the following 2 files to your shop directory:
    * `app/etc/modules/Gfe_SphinxSearch.xml`
    * `lib/sphinxapi.php`
* Logout from magento backend to execute extension setup files.
* Login to backend to configure sphinxsearch under Configuration/Catalog/Sphinx Search Engine.
![](sphinxsearch/sphinx.png)
* Recreate index catalog_search

Installing sphinx server
------------------------
Please refer to the sphinx installation guide for your server OS.

https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-sphinx-on-ubuntu-16-04
Just a quick guide for Debian GNU/Linux or Ubuntu:

* `# apt-get install sphinxsearch`
* Then (以 root 身份)copy sphinx.conf.example(本目录下的) to /etc/sphinxsearch/sphinx.conf and edit according to your database configuration.
This config file provides sphinx search server all information to create an its index for all products in your magento shop.
* Edit /etc/default/sphinxsearch (change NO to YES)
* re-create sphinx' index(没错 indexer 就是 sphinx 自带的命令，可任意目录执行，以 root 身份): `# indexer --all`

```
root@vagrant-ubuntu-trusty-64:/vagrant/httpdocs/m1932# indexer --all
Sphinx 2.0.4-id64-release (r3135)
Copyright (c) 2001-2012, Andrew Aksyonoff
Copyright (c) 2008-2012, Sphinx Technologies Inc (http://sphinxsearch.com)

using config file '/etc/sphinxsearch/sphinx.conf'...
indexing index 'fulltext'...
WARNING: Attribute count is 0: switching to none docinfo
collected 252 docs, 0.4 MB
sorted 0.1 Mhits, 100.0% done
total 252 docs, 393189 bytes
total 0.047 sec, 8204941 bytes/sec, 5258.65 docs/sec
total 2 reads, 0.000 sec, 29.0 kb/call avg, 0.1 msec/call avg
total 6 writes, 0.000 sec, 21.1 kb/call avg, 0.0 msec/call avg
```
* start sphinx search with `# /etc/init.d/sphinxsearch start`

```
root@vagrant-ubuntu-trusty-64:/vagrant/httpdocs/m1932# /etc/init.d/sphinxsearch start
Starting sphinxsearch: Sphinx 2.0.4-id64-release (r3135)
Copyright (c) 2001-2012, Andrew Aksyonoff
Copyright (c) 2008-2012, Sphinx Technologies Inc (http://sphinxsearch.com)

using config file '/etc/sphinxsearch/sphinx.conf'...
listening on all interfaces, port=9312
listening on all interfaces, port=9306
precaching index 'fulltext'
precached 1 indexes in 0.003 sec
sphinxsearch.
```



---



https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-sphinx-on-centos-7
http://sphinxsearch.com/docs/current.html#installing-redhat

Before installation make sure you have these packages installed:
```
$ yum install postgresql-libs unixODBC
```

Download RedHat RPM from [Sphinx website](http://sphinxsearch.com/downloads/release/) 
要注意看版本，你就因为下了 CentOS6.x 的给 CentOS7.x 用，导致依赖缺失报错(实际不是)，瞎折腾了半天
and install it:
```
wget http://sphinxsearch.com/files/sphinx-2.2.11-2.rhel6.x86_64.rpm
```
```
$ rpm -Uhv sphinx-2.2.1-1.rhel6.x86_64.rpm
```

After preparing configuration file, you can start searchd daemon:
```
$ service searchd start
```


/etc/sphinx/sphinx.conf 文件已存在，先备份，再拷贝
/etc/default/sphinxsearch 没有这个文件
这个在 rpm 安装后就在屏幕上打印出来了，注意看：

sphinx: /etc/sphinx /usr/share/sphinx
 
Sphinx installed!
Now create a full-text index, start the search daemon, and you're all set.
 
To manage indexes:
    editor /etc/sphinx/sphinx.conf
 
To rebuild all disk indexes:
    sudo -u sphinx indexer --all --rotate
 
To start/stop search daemon:
    service searchd start/stop

CentOS7 是
systemctl status searchd
 
To query search daemon using MySQL client:
    mysql -h 0 -P 9306
    mysql> SELECT * FROM test1 WHERE MATCH('test');
 
See the manual at /usr/share/doc/sphinx-2.2.10 for details.






Sphinx: WARNING: Attribute count is 0: switching to none docinfo

There is nothing to be scared. 
The message says that the mode of additional attributes storing, which is set in the config file, will be turned off, due to absence of these attributes.

docinfo is the complete set of per-document attribute values.

Read more about this here http://sphinxsearch.com/docs/2.0.4/attributes.html

Updated link: sphinxsearch.com/docs/current.html#attributes – Adam Jan 9 '14 at 11:44










Done!

Acknowledgements
----------------
I first integrated sphinx search into Magento 1.4.X.X with the help of this post:  
http://tonyhb.com/using-sphinx-within-magento-plus-optimising-search-result-rankings-weights-and-relevancy  
You can find this integration by user tonyhb here on github:  
https://gist.github.com/2727341  
I turned it into a full extension since then.

